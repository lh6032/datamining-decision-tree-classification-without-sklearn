
"""Author: Lingmin Hou, Sophie(lh6032) """
"""Date: Mar 07 2021 """

import pandas as pd
import numpy as np
import math
import os

# create global variables:
# 1).left_decion_order: store visited attributes in left tree
# 2).right_decision_order: store visited attributes in right tree
# 3).model: 
# uses list to store trained decision tree model, it keeps all nodes information in the decision tree,
# the structure of the model is: the first node is the root(parent), and the location of its left child node is 
# index of parent node * 2 + 1, and the location of its right child node is index of parent node * 2 + 2, and the
# left child node and right child node can be parent nodes too, their children nodes same as mentioned.
# For each node, there are 4 elements inside:
# i.the first is attribute,
# ii.the second is threshold,
# iii.the third is left predicted result, when attribute <= threshold,
# iv. the fourth is right predicted result, when attribute > threshold.
global left_decion_order, right_decision_order, model


def read_file(file_path):
    """Read training data and convert dependent variable 'Class' from categorical data to numerical data,
     use 1 to represent 'Assam', and use '-1' to represent 'Bhuttan'.
    """
    df = pd.read_csv(file_path)
    # convert "Class" to numberical labels
    class_labels = {'Assam': 1, 'Bhuttan': -1}
    df['Class'] = df['Class'].apply(lambda x: class_labels[x])
    return df


def quantize_attribute(df, attribute):
    """Quantize attributes to remove noise and create all possible thresholds.
    Args:
        df(dataframe): data
        attribute (string): one column name in the dataframe
    Returns:
        df(dataframe): cleaned data
        all_thresholds: all possiple thresholds for each attribute
    """    
    # quantize the attribute column by rounding and get values of thresholds
    if attribute == 'Age':
        all_thresholds = ((df[attribute]/2).apply(np.round) * 2).values  # binsize = 2
    elif attribute == 'Ht':
        all_thresholds = ((df[attribute]/5).apply(np.round) * 5).values  # binsize = 5
    else:
        all_thresholds = ((df[attribute]/1).apply(np.round) * 1).values  # binsize = 1
    df = df.sort_values(attribute)  # sort dataframe based on this attribute
    # print(df)  # check dataframe
    return df, all_thresholds


def bhattacharyya_coefficient(prob1, prob2):
    """Use bhattacharyya coefficient to calculate similarity between two groups in one side
    (left or right), the less the better. Lower coefficient means two groups has less overlap
    in the probability distribution.
    Args:
        prob1 (float): probability of yes_count in the all points in the left or right split.
        prob2 (float): probability of no_count in the all points in the left or right split.
    Returns: bhattacharyya coefficient: float
    """    
    return np.sum(np.sqrt(prob1 * prob2))


def gini_index(prob1, prob2):
    """Use gini index to calculate similarity between two groups in one side (left or right),
    the less the better. Lower coefficient means two groups has less overlap in the probability
    distribution.
    Args:
        prob1 (float): probability of yes_count in the all points in the left or right split.
        prob2 (float): probability of no_count in the all points in the left or right split.
    Returns: gini index: float
    """  
    return 1 - (prob1)**2 - (prob2)**2


def weighted_coefficient_with_best_threshold(df, attribute, method):
    """Calculate the weighted bhattacharyya coefficient or weighted gini index according to method and get best thresholds.
    Args:
        df(dataframe): data
        attribute (string): one column name in the dataframe
    Returns:
        best_threshold(float): best threshold in the attribute
        min_coeff(float): corresponding coefficient of the best threshold
    """    
    df, all_thresholds = quantize_attribute(df, attribute)  # quantize attributes first
    all_thresholds = sorted(set(all_thresholds))  # sort and remove duplicated thresholds
    # print('all_thresholds=', all_thresholds)  # check all possible thresholds
    all_bha_coeff_in_one_attr = {}  # store all coefficient of current attribute
    all_attribute_values = df[attribute].values  # get all values of current attribute
    all_class_values = df['Class'].values   # get all values of 'Class'

    # loop all threshold and calculate all points with all possible thresholds to get the miminum coefficient.
    for current_threshold in all_thresholds:  # get one threshold to test
        left_yes_count, left_no_count, right_yes_count, right_no_count = 0, 0, 0, 0   # store total number of yes and no in left and right sides
        for curr_point_index, curr_point_value in enumerate(all_attribute_values): # get each point from all points 
            # if point value <= threshold (left) and corresponding Class value is 1 (yes),(which is 'Assam'), then number of left yes increases;
            # if point value <= threshold (left) and corresponding Class value is -1 (no),(which is 'Bhuttan'), then number of left no increases;
            # if point value > threshold (right) and corresponding Class value is 1 (yes),(which is 'Assam'), then number of right yes increases;
            # if point value > threshold (right) and corresponding Class value is -1 (no),(which is 'Bhuttan'), then number of right no increases.
            if curr_point_value <= current_threshold:   
                if all_class_values[curr_point_index] == 1: 
                    left_yes_count += 1
                else:
                    left_no_count += 1
            else:
                if all_class_values[curr_point_index] == 1:
                    right_yes_count += 1
                else:
                    right_no_count += 1
        count_left, count_right = left_yes_count + left_no_count, right_yes_count + right_no_count  # get total counts in left and right split respectively
        # calculate 4 probabilies: left_yes_prob, left_no_prob, right_yes_prob, right_no_prob
        left_yes_prob, left_no_prob = left_yes_count/count_left if count_left != 0 else 0, left_no_count/count_left if count_left != 0 else 0
        right_yes_prob, right_no_prob = right_yes_count/count_right if count_right != 0 else 0, right_no_count/count_right if count_right != 0 else 0
        # use bhattacharyya to calculate impurity
        if method == 'bhattacharyya': 
            left_coeff = bhattacharyya_coefficient(left_yes_prob, left_no_prob)  # calculate bhattacharyya coefficient for left split
            right_coeff = bhattacharyya_coefficient(right_yes_prob, right_no_prob) # calculate bhattacharyya coefficient for right split
        # use gini to calculate impurity
        elif method == 'gini': 
            left_coeff = gini_index(left_yes_prob, left_no_prob)  # calculate gini index for left split
            right_coeff = gini_index(right_yes_prob, right_no_prob)  # calculate gini index for right split
        
        # calulate weighted coefficient
        weighted_coeff = left_coeff * (count_left/(count_left+count_right)) + right_coeff * (count_right/(count_left+count_right))
        # store all coeff inside dictionary
        all_bha_coeff_in_one_attr[current_threshold] = weighted_coeff
    best_threshold = min(all_bha_coeff_in_one_attr, key=all_bha_coeff_in_one_attr.get)
    min_coeff = min(all_bha_coeff_in_one_attr.values())
    return best_threshold, min_coeff


def predicted_results(df):
    """Get result of prediction, which can be 1("Assam"), when the number of class Asssam is
    more than class Bhuttan, otherwise the result is -1('Bhuttan').
    Args:
        df (dataframe): our data
    Returns:
        (int): class label 1 or -1
    """    
    class_A_count = len(df[df['Class'] == 1])  # count number of class Assam in the current dataframe
    class_B_count = len(df[df['Class'] == -1]) # count number of class Bhuttan in the current dataframe
    return 1 if class_A_count > class_B_count else -1


def decision_tree(df, all_attributes, attri_loc, best_attribute, left_decision_order, right_decision_order, model, method):
    """Build decision tee. Iterate the dataframe to find node until one of three conditions satisfied.
    First get the best threshold for each attribute, and then compare all of the best thresholds from 
    all attributes. Based on the miminum coefficient value, we can get the best attribute. And then use
    the best attribute and its best threshold to split the original dataframe into two parts, left and right.
    The left dataframe is filtered via best attribute <= threshold, the right dataframe is filterd via 
    best attribute > threshold. Iterate the previous steps inside left and right dataframes respectively
    to get nodes. And then store each node information inside model.

    Args:
        df (dataframe): our data, this will be filtered based on best attribute and its threshold.
        all_attributes (list): all attributes name.
        attri_loc (int): the index of node in model.
        best_attribute (string): the attribute with the lowest coefficient value.
        left_decision_order (list)): to track visited atributes for left split.
        right_decision_order (list): to track visited atributes for right split.
        model ([]): to store our decision tree model
        method (str, optional): can be 'gini' or 'bhattacharyya'. Defaults to 'gini'.
    """    
    # calculate portions of each class
    class_A_portion, class_B_portion = len(df[df['Class']==1])/len(df['Class']) if len(df['Class']) else 0, len(df[df['Class']==-1])/len(df['Class']) if len(df['Class']) else 0
    # decision_order to track the tree depth
    decision_order = left_decision_order if left_decision_order else right_decision_order
    
    # Base case: stop iteration when only 9 points left in the dataframe or exceed max depth(default:8) or one class takes up 95%
    if len(df) < 9 or len(decision_order) > (2**8)/2 or (class_A_portion > 0.95) or (class_B_portion > 0.95):
        return

    min_coeff_parent_attribute = float('inf')  # initialize minimum coefficient
    best_threshold_parent_attribute = 0.0   # initialize best threshold
    for each_attribute in all_attributes:
        best_threshold_curr_attr, min_coeff_curr_attr = weighted_coefficient_with_best_threshold(df, each_attribute, method) # call func to get miminum coefficient value and best threshold for each attribute
        if min_coeff_curr_attr < min_coeff_parent_attribute:    # comapre all attributes to get the miminum coefficient value and best threshold, best attribute
            min_coeff_parent_attribute = min_coeff_curr_attr    
            best_threshold_parent_attribute = best_threshold_curr_attr
            best_attribute = each_attribute
    left_decision_order.extend([best_attribute, best_threshold_parent_attribute])  # track tree depth of left side
    right_decision_order.extend([best_attribute, best_threshold_parent_attribute])  # track tree depth of right side
    # split data into left and right parts by the root attribute and its best threshold
    left_df = df[df[best_attribute] <= best_threshold_parent_attribute]  # split dataframe and get left part
    # print('left_df')
    # print(left_df)    # uncomment to check left dataframe
    right_df = df[df[best_attribute] > best_threshold_parent_attribute]   # split dataframe and get right part
    # print('right_df')
    # print(right_df)   # uncomment to check right dataframe
    left_predicted = predicted_results(left_df)  # call func to get the predicted result on left node
    right_predicted = predicted_results(right_df)  # call func to get the predicted result on right node
    # store node information inside model
    model[attri_loc] = [best_attribute, best_threshold_parent_attribute, left_predicted, right_predicted]
    # iterate in left datarame
    decision_tree(left_df, all_attributes, attri_loc*2+1, best_attribute, left_decision_order, [], model, method)
    # iterate in right dataframe
    decision_tree(right_df, all_attributes, attri_loc*2+2, best_attribute, [], right_decision_order, model, method)


def apply_model_to_predict(model, each_row, attri_idx):
    """Apply model to get result of prediction, which is stored inside model at the third and
    fourth positions. The third position is left side prediction, and the fourth one is right
    side prediction.

    Args:
        model ([]): trained decision tree model
        each_row (dataframe): one row of dataframe
        attri_idx (int)): node index in model
    Returns:
        [int]: predictied result: 1 or -1
    """    
    # Base case: if leaf node or exceed max depth of decision tree, return parent node to check result
    if attri_idx >= len(model) or not model[attri_idx]:
        parent_loc = math.ceil(attri_idx/2) - 1    # get parent node location
        result_index_in_model = attri_idx % 2      # if 0, then go to left result; if 1, then go to right result
        return model[parent_loc][result_index_in_model + 2] # get prediction result

    # retrieve related model information from model, check model from root node to leaf node
    target_attribute, target_threshold, left_result, right_result = model[attri_idx]
    # if the value <= threshold, iterating checking the left results
    if each_row[target_attribute] <= target_threshold:
        return apply_model_to_predict(model, each_row, attri_idx * 2 + 1)
    else:     # else checking the right results
        return apply_model_to_predict(model, each_row, attri_idx * 2 + 2)

    
def test_model(model, test_data, attri_loc):
    """Test model with 10% of training data and calculate the accuracy.
    Args:
        model ([]): trained decision tree model
        test_data (dataframe): 10% of training data 
        attri_loc ([int]): node index in model
    """    
    TP, FP, TN, FN = 0, 0, 0, 0
    for index_row, each_row in test_data.iterrows():
        pred = apply_model_to_predict(model, each_row, 0)  # call func to get predicted result
        # print(pred)  # uncomment to check predicted result.
        # if predicted result is 1 and actual result is 1, then true positive(TP) increases.
        # if predicted result is 1 and actual result is -1, then false positive(FP) increases.
        # if predicted result is -1 and actual result is 1, then false negative(FN) increases.
        # if predicted result is -1 and actual result is -1, then true negative(TN) increases.
        if pred == 1:
            if each_row['Class'] == 1:
                TP += 1
            else:
                FP += 1
        else:
            if each_row['Class'] == 1:
                FN += 1
            else:
                TN += 1
    accuracy = (TP + TN) / (TP + TN + FP + FN)  # calculate accuracy
    print("\nAccuracy = ", accuracy) 
    print("\nConfusion Matrix: ")
    print("                                  Actual Values")
    print("                                 Positive  |   Negative")
    print("Predicted Values:  Positive  |TP: ", TP, "        FP: ", FP)
    print("                   Negative  |FN: ", FN, "         TN: ", TN)
   


def create_subprogram_test_model(model, validation_file_path):
    """Creates sub program to test mdoel on validation data and get predicted results and
     write out results to csv file.
    """
    # create sub program
    with open('Trained_Classifier.py', 'w') as file:  
        # import packages
        file.write("import pandas as pd\n") 
        file.write("import numpy as np\n")
        file.write("import math\n")
        # read vadation data
        file.write("validation_df = pd.read_csv('" + validation_file_path + "')\n")
        # create function apply_model_to_predict() to apply trained model and return predicted results
        file.write("def apply_model_to_predict(model, each_row, attri_loc, indent):\n")
        file.write("    indent += '     '\n")
        file.write("    if attri_loc >= len(model) or not model[attri_loc]:\n")
        file.write("        parent_loc = math.ceil(attri_loc/2) - 1    # get parent node location:\n")
        file.write("        result_index_in_model = attri_loc % 2      # if 0, then go to left result; if 1, then go to right result:\n")
        file.write("        print(indent, model[parent_loc][result_index_in_model + 2])\n")
        file.write("        return model[parent_loc][result_index_in_model + 2]\n")
        file.write("    target_attribute, target_threshold, left_result, right_result = model[attri_loc]\n")
        file.write("    if each_row[target_attribute] <= target_threshold:\n")
        file.write("        print(indent+'if '+target_attribute + '<=' + str(target_threshold) + ':')\n")
        file.write("        return apply_model_to_predict(model, each_row, attri_loc * 2 + 1, indent)\n")
        file.write("    else:     # else checking the right results:\n")
        file.write("        print(indent+'else:')\n")
        file.write("        return apply_model_to_predict(model, each_row, attri_loc * 2 + 2, indent)\n")
        # create function validate_model() to iterate through validation dataset and get result for all rows
        file.write("def validate_model(model, test_data, attri_loc):\n")
        file.write("    pred_results = []\n")
        file.write("    for index_row, each_row in test_data.iterrows():\n")
        file.write("        pred = apply_model_to_predict(model, each_row, 0, '')\n") # call func to get predicted result for current row
        file.write("        pred_results.append(pred)\n")
        file.write("    print(*pred_results, sep='\\n')  # print predicted results\n")
        file.write("    return pred_results\n")
        file.write("pred_results = validate_model(" + str(model) + ", validation_df, 0)\n")  # call func validate_model()
        # file.write("print('Predicted Results on Validation Dataset: ', pred_results)\n")
        # write out results inside a csv file
        file.write("with open('MyClassifications.csv', 'w') as file_out:\n")
        file.write("    np.savetxt(file_out, pred_results, fmt='%d', delimiter=',')\n")
 

def run_subprogram():
    """Creates command to run the subprogram.
    """
    os.system("python Trained_Classifier.py")

def main():
    file_path = '../Abominable_Data.csv'   # training data file path
    df = read_file(file_path) 
    all_attributes = list(df.columns[:-1])  # get attributes of dataframe, except the 'Class'
    left_decion_order, right_decision_order = [], []
    model = [None, None, None, None] * 2**8
    method = 'bhattacharyya'  # calculate node impurity methods, it can be ‘bhattacharyya' or 'gini
    # method = 'gini'   # uncomment to test gini
    print("Calculate Node Impurity Method is: ", method)
    decision_tree(df, all_attributes, 0, '', left_decion_order, right_decision_order, model, method)
    print(model) # uncomment to print our trained model
    test_data = df.iloc[-100:,]   # get 10% of original dataframe to test
    test_model(model, test_data, 0)  # get accuracy of test data
    # validate the model on validation dataset
    validation_file_path = '../Abominable_Validation_Data_for_Testing.csv' # file path of validation dataset
    create_subprogram_test_model(model, validation_file_path) # create sub progarm
    run_subprogram()  # run the sub program


if __name__ == '__main__':
    main()  # driver